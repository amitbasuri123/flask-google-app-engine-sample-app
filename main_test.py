import unittest
import main


class TestServer(unittest.TestCase):

    def test_index(self):
        main.app.testing = True
        client = main.app.test_client()

        r = client.get('/')
        assert r.status_code == 200
        # assert 'Hello World' in r.data.decode('utf-8')


if __name__ == '__main__':
    unittest.main()
